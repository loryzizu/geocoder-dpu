### Descrizione

Questa DPU permette di geocodificare i dati contenuti nei file di input.
I formati dei file di input accettati dalla DPU sono CSV e RDF.
I servizi di geocodifica utilizzati sono Google Maps Geocoding API e What3Words.

Per quanto riguarda i file CSV, l'utente deve indicare (tramite configurazione della DPU) gli indici delle colonne i cui valori, uniti tra loro, formano l'indirizzo di ciascuna riga. Ogni riga avente un indirizzo viene geocodificata, computando longitudine e latitudine. Tali valori vengono appesi nelle ultime due colonne di ogni riga oppure inseriti nelle colonne con indice fornito dall'utente nella configurazione della DPU.

Per i file RDF in modo analogo, vanno indicati i predicati i cui oggetti, uniti tra loro, formano l'indirizzo delle entità. La longitudine e la latitudine computate vengono aggiunte, per ogni entità con un indirizzo valido, come oggetto dei predicati indicati dall'utente nella configurazione della DPU. 



E' necessario fornire una Google Maps Geocoding API Key. Se i limiti giornalieri di uso vengono superati viene utilizzato il servizio What3Words.

### Parametri

| Nome | Descrizione |
|:----|:----|
|Generale||
|Google Maps Geocoding API Key | Google Maps Geocoding API Key. https://developers.google.com/maps/documentation/geocoding/get-api-key|
|Sostituisci |Permette di decidere se ignorare gli elementi che possiedono già longitudine e latitudine o meno.|
|CSV||
|Indici colonne indirizzo|Indici delle colonne che compongono l'indirizzo, separati da virgola|
|Indice colonna latitudine|Indice della colonna che contiene la latitudine (sia in input che in output)|
|Indice colonna longitudine|Indice della colonna che contiene la longitudine (sia in input che in output)|
|Separatore|Seperatore del file CSV in input|
|Il CSV ha uno header| Selezionare se il CSV ha una riga di intestazione|
|RDF||
|URIs predicati indirizzo| URI dei predicati i cui oggetti compongono l'indirizzo. Separati da virgola.|
|URI predicato latitudine| URI del predicato il cui oggetto contiene la latitudine (sia in input che in output)|
|URI predicato longitudine| URI del predicato il cui oggetto contiene la longitudine (sia in input che in output)|

### Inputs and outputs

|Nome |Tipo | DataUnit | Descrizione |
|:--------|:------:|:------:|:-------------|
|input |i |FilesDataUnit |File in input (uno o molti) |
|gecocodedResourcesFile |o |WritableFilesDataUnit |File di origine con risorse geocodificate |
|discardedResourcesFile |o |WritableFilesDataUnit |File con le sole risorse non geocodificate (funziona solo con RDF) |
