package umbriadigitale.unifiedviews.dpu.geocoder;

import com.hp.hpl.jena.rdf.model.*;
import com.hp.hpl.jena.shared.InvalidPropertyURIException;
import eu.unifiedviews.dataunit.DataUnit;
import eu.unifiedviews.dataunit.DataUnitException;
import eu.unifiedviews.dataunit.files.FilesDataUnit;
import eu.unifiedviews.dataunit.files.WritableFilesDataUnit;
import eu.unifiedviews.dpu.DPU;
import eu.unifiedviews.dpu.DPUException;
import eu.unifiedviews.helpers.dataunit.files.FilesDataUnitUtils;
import eu.unifiedviews.helpers.dpu.config.ConfigHistory;
import eu.unifiedviews.helpers.dpu.context.ContextUtils;
import eu.unifiedviews.helpers.dpu.exec.AbstractDpu;
import eu.unifiedviews.helpers.dpu.extension.ExtensionInitializer;
import eu.unifiedviews.helpers.dpu.extension.faulttolerance.FaultTolerance;
import eu.unifiedviews.helpers.dpu.extension.faulttolerance.FaultToleranceUtils;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import umbriadigitale.unifiedviews.dpu.geocoder.beans.LatLng;
import umbriadigitale.unifiedviews.dpu.geocoder.exception.GoogleMapOverQueryLimitException;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Main data processing unit class.
 *
 * @author <a href="lorenzoranucci.com">Lorenzo Ranucci</a>
 */
@DPU.AsTransformer
public class GeoCoder extends AbstractDpu<GeoCoderConfig_V1> {
	private static int cntRequest=0;
	private static int cntRequestOK=0;
	private static int cntRequestKO=0;


	private static final Logger LOG = LoggerFactory.getLogger(GeoCoder.class);
	/*
	 * Replace cases
	 * */
	private static final int REPLACE_VALID=0;
	private static final int NOT_REPLACE_VALID=1;
	private static final int NOT_REPLACE_NOT_VALID=2;

	@DataUnit.AsInput(name = "Input")
	public FilesDataUnit filesInput;

	@DataUnit.AsOutput(name = "gecocodedResourcesFile")
	public WritableFilesDataUnit filesOutput;

	@DataUnit.AsOutput(name = "discardedResourcesFile")
	public WritableFilesDataUnit filesOutput2;


	@ExtensionInitializer.Init
	public FaultTolerance faultTolerance;

	public GeoCoder() {
		super(GeoCoderVaadinDialog.class, ConfigHistory.noHistory(GeoCoderConfig_V1.class));
	}

	@Override
	protected void innerExecute() {

		LOG.info("Starting ...");
		ContextUtils.sendShortInfo(ctx, "GeoCoder.message");
		try {
			// process every input file
			final List<FilesDataUnit.Entry> files = FaultToleranceUtils.getEntries(faultTolerance, filesInput,
					FilesDataUnit.Entry.class);
			
			for (final FilesDataUnit.Entry entry : files) {
				String uriString = entry.getFileURIString().replace("file:", "");
				String path = null;
				try {
					path = URLDecoder.decode(new File(uriString).getAbsolutePath(),"UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				// Check file extension and generate output symbolic name
				String outputFileName = "NewFile";
				String fileExtension="";
				int pos = entry.getSymbolicName().lastIndexOf(".");
				if (pos > 0) {
					outputFileName = entry.getSymbolicName().substring(0, pos);
					fileExtension= entry.getSymbolicName().substring(pos, entry.getSymbolicName().length());
				}
				
				//skip file if is not rdf or csv
				if(!fileExtension.equalsIgnoreCase(".rdf") && !fileExtension.equalsIgnoreCase(".csv")){
					ContextUtils.sendWarn(ctx, "GeoCoder.warning",
							"GeoCoder.warning.details",
							"Skipped file: "+path+".\n It isn't CSV or RDF");
					continue;
				}

				ArrayList<String> outputFilePaths;
				if(fileExtension.contentEquals(".rdf")){
					ContextUtils.sendInfo(ctx, "GeoCoder.readingFile", "GeoCoder.readingFile.details", path);
					ContextUtils.sendInfo(ctx, "GeoCoder.debug", "GeoCoder.debug", "AddressPropertiesURIs: "+this.config.getAddressPropertiesURIs());
					ContextUtils.sendInfo(ctx, "GeoCoder.debug", "GeoCoder.debug", "LatPropertyURI: "+this.config.getLatPropertyURI());
					ContextUtils.sendInfo(ctx, "GeoCoder.debug", "GeoCoder.debug", "LngPropertyURI: "+this.config.getLngPropertyURI());
					ContextUtils.sendInfo(ctx, "GeoCoder.debug", "GeoCoder.debug", "Replace: "+this.config.isReplace());
					ContextUtils.sendInfo(ctx, "GeoCoder.debug", "GeoCoder.debug", "APIKey: "+this.config.getAPIKey());
					ContextUtils.sendInfo(ctx, "GeoCoder.debug", "GeoCoder.debug", "Lat max: "+this.config.getLatMax());
					ContextUtils.sendInfo(ctx, "GeoCoder.debug", "GeoCoder.debug", "Lat min: "+this.config.getLatMin());
					ContextUtils.sendInfo(ctx, "GeoCoder.debug", "GeoCoder.debug", "Lng max: "+this.config.getLngMax());
					ContextUtils.sendInfo(ctx, "GeoCoder.debug", "GeoCoder.debug", "Lng min: "+this.config.getLngMin());

					outputFilePaths=geocodeRDF(path, this.config.getAddressPropertiesURIs(),
							this.config.getLatPropertyURI(), this.config.getLngPropertyURI(), 
							this.config.isReplace(),  this.config.getAPIKey(),
							this.config.getLatMax(),this.config.getLatMin(), this.config.getLngMax(),this.config.getLngMin());
				}
				else{
					ContextUtils.sendInfo(ctx, "GeoCoder.readingFile", "GeoCoder.readingFile.details", path);
					ContextUtils.sendInfo(ctx, "GeoCoder.debug", "GeoCoder.debug", "address col idxs: "+this.config.getAddressColumnsIndexes());
					ContextUtils.sendInfo(ctx, "GeoCoder.debug", "GeoCoder.debug", "lat col idx: "+this.config.getLatColumnIndex());
					ContextUtils.sendInfo(ctx, "GeoCoder.debug", "GeoCoder.debug", "lng col idx: "+this.config.getLngColumnIndex());
					ContextUtils.sendInfo(ctx, "GeoCoder.debug", "GeoCoder.debug", "separator: "+this.config.getSeparator());
					ContextUtils.sendInfo(ctx, "GeoCoder.debug", "GeoCoder.debug", "header: "+this.config.hasHeader());
					ContextUtils.sendInfo(ctx, "GeoCoder.debug", "GeoCoder.debug", "Replace: "+this.config.isReplace());
					ContextUtils.sendInfo(ctx, "GeoCoder.debug", "GeoCoder.debug", "APIKey: "+this.config.getAPIKey());
					
					outputFilePaths=geocodeCSV(path, this.config.getAddressColumnsIndexes(),
							this.config.getLatColumnIndex(), this.config.getLngColumnIndex(),
							this.config.getSeparator(), this.config.hasHeader(), 
							this.config.isReplace(), this.config.getAPIKey());
				}
				/*
				 * skip writing file output if input file is not geocoded
				 * */
				if(outputFilePaths==null || outputFilePaths.size()==0){
					continue;
				}

				int cnt=0;
				for(String outputFilePath: outputFilePaths){
					File outputFile = new File(outputFilePath);

					final WritableFilesDataUnit filesOutputPtr;
					if(cnt==1){//discarded resources file
						outputFileName+="_DiscardedFromGeocoder"+fileExtension;
						filesOutputPtr=filesOutput2;
					}
					else{
						outputFileName += "_Geocoded"+fileExtension;
						filesOutputPtr=filesOutput;
					}

					final String outputFileNameFinal = outputFileName;

					if (outputFile.exists() && !outputFile.isDirectory()) {
						final FilesDataUnit.Entry destinationFile = faultTolerance
								.execute(new FaultTolerance.ActionReturn<FilesDataUnit.Entry>() {
									@Override
									public FilesDataUnit.Entry action() throws Exception {
										return FilesDataUnitUtils.createFile(filesOutputPtr, outputFileNameFinal);
									}
								});

						// copy output file
						try {
							ContextUtils.sendInfo(ctx, "GeoCoder.debug", "GeoCoder.debug",
									"Output path: "+outputFile.getAbsolutePath());
							FileUtils.copyInputStreamToFile(new FileInputStream(outputFile),
									FaultToleranceUtils.asFile(faultTolerance, destinationFile));
						} catch (IOException ex) {
							throw ContextUtils.dpuException(ctx, ex, "FilesDownload.execute.exception");
						}
					}
					cnt++;
				}

			}
		} catch (DataUnitException e) {
			ContextUtils.sendWarn(ctx, "GeoCoder.debug", "GeoCoder.debug", e.getMessage());
		} catch (DPUException e) {
			e.printStackTrace();
			ContextUtils.sendWarn(ctx, "GeoCoder.debug", "GeoCoder.debug", e.getMessage());
		}

	}
	
	private ArrayList<String> geocodeRDF(String path, String addressPropertiesUris, String latPropertyUri, String lngPropertyUri,
										 boolean replace, String APIKey,
										 String latMax, String latMin, String lngMax, String lngMin
	) {
		// create an empty model
		Model model = ModelFactory.createDefaultModel();
		Model modelDiscarded = ModelFactory.createDefaultModel();
		File inFile = new File(path);
		FileInputStream in;
		try {
			in = new FileInputStream(path);
			// read the RDF/XML file
			model.read(in, null);
			ContextUtils.sendInfo(ctx, "GeoCoder.debug", "GeoCoder.debug", 
					"latURI: "+latPropertyUri+"\n lngURI: "+lngPropertyUri);
			//parseAddressUris and get all the resources with all the properties
			String[] addressUris=addressPropertiesUris.split(",");
			
			/*
			 * ***********************************************************************
			 * ***********************************************************************
			 * *****************GEOCODE every resource***********************
			 * ***********************************************************************
			 * ***********************************************************************
			 * */
			Property latProperty = model.createProperty(latPropertyUri);
			Property lngProperty = model.createProperty(lngPropertyUri);
			ResIterator iterator = model.listSubjects();
			
			/*
			 * iterate over every rdf resource
			 * */
			while (iterator.hasNext()) {
				Resource resource = iterator.next();
				String dummyObject = null;
				/* ***********************************************************************
				 * *****Compose full address with all user-selected properties values*****
				 * ***********************************************************************
				 * */
				String fullAddress="";
				int cntProperties=0;
				for(String addressPropertyUriJ:addressUris ){
					addressPropertyUriJ=addressPropertyUriJ.trim();
					if(!addressPropertyUriJ.isEmpty()){
						try{
							Property addressPropertyJ = model.createProperty(addressPropertyUriJ);
							Selector addressPropertySelector= new SimpleSelector(resource, addressPropertyJ, dummyObject);
							List<Statement> addressPropertyStatementsList = model.listStatements(addressPropertySelector).toList();
							if(addressPropertyStatementsList!=null && !addressPropertyStatementsList.isEmpty()){
								if(cntProperties==0){
									fullAddress+=addressPropertyStatementsList.get(0).getString();
									cntProperties++;
								}
								else{
									fullAddress+=" , "+addressPropertyStatementsList.get(0).getString();
								}
							}
						}
						catch (InvalidPropertyURIException e) {
							ContextUtils.sendWarn(ctx, "GeoCoder.warning",
									"GeoCoder.warning.details",
									"Invalid property: "+addressPropertyUriJ);
							continue;
						}
					}
				}
				if(fullAddress.isEmpty())
					continue;
				
				/* ***********************************************************************
				 * *****END Compose full address with all user-selected properties values*****
				 * ***********************************************************************
				 * */
				
				//retrieve existing lat and lng statements
				Selector selectorLat = new SimpleSelector(resource, latProperty, dummyObject);
				List<Statement> existingLatStatementsList = model.listStatements(selectorLat).toList();
				Selector selectorLng = new SimpleSelector(resource, lngProperty, dummyObject);
				List<Statement> existingLngStatementsList = model.listStatements(selectorLng).toList();
				
				boolean alreadyHasLat=!existingLatStatementsList.isEmpty();
				boolean alreadyHasLng=!existingLngStatementsList.isEmpty();
				
				//skip resource if already has lat & lng and replace flag false
				if(!replace && alreadyHasLat && alreadyHasLng)
					continue;
				
				/* ***********************************************************************
				 * ***************************Call Google maps API************************
				 * ***********************************************************************
				 * */
				LatLng latLng;
				try{latLng = geocodingAddressGoogle(fullAddress, APIKey);}
				catch (GoogleMapOverQueryLimitException e) {
					/* ***********************************************************************
					 * ***************************Call What3Words  API************************
					 * ***********************************************************************
					 * */
					ContextUtils.sendWarn(ctx, "GeoCoder.warning",
							"GeoCoder.warning.details",
							"Google query limit exceeded, skip");
					break;
				}
				if(latLng==null || !isLatLngInsideLimits(latLng,latMax, latMin, lngMax, lngMin)){
					latLng=geocodingAddressOpenCage(fullAddress, "8e8d96938e5e8a83e05166164666e3aa");
				}

				if(latLng!=null && isLatLngInsideLimits(latLng,latMax, latMin, lngMax, lngMin)){
					//set geocoded lat only if replace flag is true or resource does not already have lat 
					if(replace || !alreadyHasLat){
						//delete existing lat statement
						if(replace && alreadyHasLat){
							model.remove(existingLatStatementsList);
						}
						
						//add new lat statement
						Statement latStatement = model.createStatement(resource, latProperty, String.valueOf(latLng.getLat()));
						model.add(latStatement);
					}
					
					//set geocoded lng only if replace flag is true or resource does not already have lng
					if(replace || !alreadyHasLng){
						//delete existing lng statement
						if(replace && alreadyHasLng){
							model.remove(existingLngStatementsList);
						}
						
						//add new lng statement
						Statement lngStatement = model.createStatement(resource, lngProperty, String.valueOf(latLng.getLng()));
						model.add(lngStatement);
					}
				}
				else{//add to discarded
					Selector selectorResourceStatements = new SimpleSelector(resource, null, dummyObject);
					List<Statement> allStatementsResource=model.listStatements(selectorResourceStatements).toList();
					modelDiscarded.add(allStatementsResource);
				}
			}
			ArrayList<String> result=new ArrayList<String>();
			String inputFileName = inFile.getName();
			String outputFileName = "NewFile";
			String outputFileName2 = "NewFile";
			int pos = inputFileName.lastIndexOf(".");
			if (pos > 0) {
				outputFileName = outputFileName2 = inputFileName.substring(0, pos);
			}
			outputFileName += "_Geocoded.rdf";
			outputFileName2 += "_DiscardedFromGeocoder.rdf";

			// create output file
			File outFile = new File(inFile.getParent() + "/"+outputFileName);
			if(outFile.exists() && !outFile.isDirectory()){
				outFile.delete();
			}
			outFile.createNewFile();
			FileOutputStream fos= new FileOutputStream(outFile);
			model.write(fos);
			result.add(outFile.getAbsolutePath());

			// create output file2, discarded
			File outFile2 = new File(inFile.getParent() + "/"+outputFileName2);
			if(outFile2.exists() && !outFile2.isDirectory()){
				outFile2.delete();
			}
			outFile2.createNewFile();
			fos= new FileOutputStream(outFile2);
			modelDiscarded.write(fos);
			result.add(outFile2.getAbsolutePath());

			return result;
		} catch (IOException e) {
			e.printStackTrace();
			ContextUtils.sendWarn(ctx, "GeoCoder.debug", "GeoCoder.debug", e.getMessage());
			return null;
		}
	}


	private ArrayList<String> geocodeCSV(String path,String addressColumnsIndexes, String latColumnIndex, String lngColumnIndex, String separator, boolean hasHeader, boolean replace, String APIKey) {
		/*
		 * Reading and checking input file
		 * */
		Scanner scanner;
		File inputFile;
		String firstLine="";
		//csv number of columns is assumed from the first row
		int csvColumns=-1;
		try {
			inputFile = new File(path);
			scanner = new Scanner(inputFile);
			if(!scanner.hasNextLine()){
				//skip if file is empty
				ContextUtils.sendWarn(ctx, "GeoCoder.warning", "GeoCoder.warning.details", "File is empty and will be skipped");
				scanner.close();
				return null;
			}	
			firstLine=scanner.nextLine();
			csvColumns=CSVManager.parseLine(firstLine,separator.charAt(0)).size();
		}
		catch (FileNotFoundException e) {
			ContextUtils.sendWarn(ctx, "GeoCoder.error", "GeoCoder.error.details", "File not found");
			return null;
		}
		/*
		 * Check separator
		 * */
		if(separator==null || separator.isEmpty() ){
			separator=",";
		}
		else{
			separator=separator.trim();
			//separator must be only one character
			if(separator.length()>1){
				separator=",";
			}
		}
		
		/*
		 * +++++++++++++++++++++++++++++++++
		 * ++++++++++Check REPLACE++++++++++
		 * +++++++++++++++++++++++++++++++++
		 * */
		
		/*
		 * If replace= true
		 * 		lat e lng index devono essere valorizzati
		 * else
		 * 		if lat e lng sono valorizzati e validi
		 * 			cerco lat e long solo se la riga non li ha già
		 * 		else
		 * 			appendo lat e long alla fine
		 * */
		
		/*
		 * 0-replace && valid latIdx e lngIdx 
		 * 1-!replace && valid latIdx e lngIdx
		 * 2-!replace && !valid or empty latIdx e lngIdx 
		 * */
		

		//check if lat and lng indexes are integer and are not out of bound
		int replaceCase = -1;
		boolean validLatLngIdx=false;
		int latIndex=-1;
		int lngIndex=-1;
		try{
			latIndex=Integer.parseInt(latColumnIndex);
			lngIndex=Integer.parseInt(lngColumnIndex);
			if(latIndex<csvColumns && lngIndex<csvColumns)
				validLatLngIdx=true;
		}
		catch (NumberFormatException e) {
			validLatLngIdx=false;
		}
		
		if(replace && !validLatLngIdx){
			ContextUtils.sendError(ctx, "GeoCoder.error", "GeoCoder.error.details", "Replace is set, but lat e lng columns indexes are not valid, file will be skipped.");
			scanner.close();
			return null;
		}
		else if(replace && validLatLngIdx){
			replaceCase=REPLACE_VALID;
		}
		else if(!replace && validLatLngIdx){
			replaceCase=NOT_REPLACE_VALID;
		}
		else if(!replace && !validLatLngIdx){
			replaceCase=NOT_REPLACE_NOT_VALID;
		}
		
		/*
		 * +++++++++++++++++++++++++++++++++
		 * ++++++++End Check REPLACE++++++++
		 * +++++++++++++++++++++++++++++++++
		 * */
		
		
		BufferedWriter bw = null;
		try {

			// create output file
			String inputFileName = inputFile.getName();
			String outputFileName = "NewFile";
			int pos = inputFileName.lastIndexOf(".");
			if (pos > 0) {
				outputFileName = inputFileName.substring(0, pos);
			}
			outputFileName += "_Geocoded.csv";
			String outputFilePath = inputFile.getParent() +"/"+ outputFileName;
			File outputfile = new File(outputFilePath);
			outputfile.createNewFile();

			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputfile)));
			
			//retrieve address columns indexes
			List<Integer> addressColumnsIndexesList=new ArrayList<>();
			for(String addressColumnIndex: addressColumnsIndexes.split(",")){
				try{
					Integer currIndex=Integer.parseInt(addressColumnIndex.trim());
					addressColumnsIndexesList.add(currIndex);
				}
				catch (NumberFormatException e) {
				}
			}
			if(addressColumnsIndexesList.isEmpty()){
				ContextUtils.sendError(ctx, "GeoCoder.error", "GeoCoder.error.details", "Address columns indexes are not valid, skipping file");
				return null;
			}

			// skip header if exists
			if (hasHeader) {
				String rawLine = firstLine;
				String outLine="";
				//add lat and lng columns in the header only in the NOT_REPLACE_NOT_VALID case
				if(replaceCase==NOT_REPLACE_NOT_VALID){
					outLine = rawLine + separator + "lat" + separator + "lng" + "\n";
				}
				bw.write(outLine);
			}
			/*
			 * 1-Read every csv line 
			 * 2-compose full address from columns
			 * 3-geocode address
			 * 4-write outout line
			 */
			while (scanner.hasNextLine()) {
				String rawLine = scanner.nextLine();
				List<String> line = CSVManager.parseLine(rawLine,separator.charAt(0));
				/*
				 * skip line if has not the correct number of columns
				 * usually when line is empty
				 * */
				if(line.size()!=csvColumns){
					bw.write(rawLine+"\n");
					continue;
				}
				
				/*
				 * retrieve old lat and lng values
				 * */
				String oldLat;
				String oldLng;
				try{
					oldLat=line.get(latIndex).trim();
					oldLng=line.get(lngIndex).trim();
				}
				catch (IndexOutOfBoundsException e) {
					oldLat="";
					oldLng="";
				}
				
				
				
				if(replaceCase==NOT_REPLACE_VALID){
					Double oldLatDouble;
					Double oldLngDouble;
					try{
						oldLatDouble=Double.parseDouble(oldLat);
						oldLngDouble=Double.parseDouble(oldLng);
					}
					catch(NullPointerException | NumberFormatException e){
						oldLatDouble= Double.parseDouble("0");
						oldLngDouble= Double.parseDouble("0");
					}
					/*
					 * if old lat and lng are valid, write old line and continue to next line
					 * */
					if(oldLatDouble.doubleValue() != 0 && oldLngDouble.doubleValue() != 0){
						bw.write(rawLine+"\n");
						continue;
					}
				}
				
				String fullAddress="";
				int cntCol=0;
				for(Integer addressColumnIndex: addressColumnsIndexesList){
					if(cntCol==0){
						fullAddress=line.get(addressColumnIndex.intValue());
						cntCol++;
						continue;
					}
					fullAddress+=" , "+line.get(addressColumnIndex.intValue());
					cntCol++;
				}
				
				/* ***********************************************************************
				 * ***************************Call Google maps API************************
				 * ***********************************************************************
				 * */
				LatLng latLng;
				try{latLng = geocodingAddressGoogle(fullAddress, APIKey);}
				catch (GoogleMapOverQueryLimitException e) {
					/* ***********************************************************************
					 * ***************************Call What3Words  API************************
					 * ***********************************************************************
					 * */
					ContextUtils.sendWarn(ctx, "GeoCoder.warning",
							"GeoCoder.warning.details",
							"Google query limit exceeded, skip");
					latLng = null;
				}
				
				String outLine="";
				
				if(latLng==null){
					latLng=new LatLng(oldLat, oldLng);
				}
				
				if(replaceCase==NOT_REPLACE_NOT_VALID){
					//append to the end of line
					outLine = rawLine + separator + latLng.getLat() + separator
							+ latLng.getLng() + "\n";
				}
				else{
					/*
					 * "Replace, valid latlng" and "not replace, valid latlng" cases
					 * Set lat and lng columns with indexes specified by user 
					 * */
					int cntCols=0;
					for(String col : line){
						if(cntCols==0){
							if(cntCols==latIndex){
								outLine=latLng.getLat();
							}
							else if(cntCols==lngIndex){
								outLine=latLng.getLng();
							}
							else{
								outLine=col;
							}
						}
						else{
							if(cntCols==latIndex){
								outLine+=","+latLng.getLat();
							}
							else if(cntCols==lngIndex){
								outLine+=","+latLng.getLng();
							}
							else{
								outLine+=","+col;
							}
						}
						cntCols++;
					}
					outLine+="\n";
				}
				
				bw.write(outLine);
			}
			scanner.close();
			ArrayList<String> result=new ArrayList<>();
			result.add(outputFilePath);
			return result;
		} catch (IOException e) {
			return null;
		} finally {
			scanner.close();
			if (bw != null)
				try {
					bw.close();
				} catch (IOException e) {
				}
		}
	}
	
	

	private LatLng geocodingAddressGoogle(String fullAddress, String APIKey) throws GoogleMapOverQueryLimitException {
		try {
			String query = "address=" + fullAddress + "&key=" + APIKey;
			URI uriGoogleAPI = new URI("https", "maps.googleapis.com", "/maps/api/geocode/json", query, null);

			URL urlGoogleAPI = uriGoogleAPI.toURL();
//			try {
//				Thread.sleep(500);
//			} catch (InterruptedException e) {
//			}
			HttpURLConnection conn = (HttpURLConnection) urlGoogleAPI.openConnection();
			conn.setRequestMethod("GET");
			InputStream responseIS;
			try {
				responseIS = conn.getInputStream();
			} catch (IOException e) {
				return null;
			}

			StringBuilder result = new StringBuilder();
			BufferedReader rd = new BufferedReader(new InputStreamReader(responseIS));
			String line;
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			rd.close();

			JSONObject jsonResult = new JSONObject(result.toString());
			JSONArray results = jsonResult.getJSONArray("results");
			//looking for error
			if (jsonResult.getString("status").equals("OVER_QUERY_LIMIT")) {
				throw new GoogleMapOverQueryLimitException();
			}

			JSONObject result0 = results.getJSONObject(0);
			if (result0 != null) {
				JSONObject geometry = result0.getJSONObject("geometry");
				JSONObject location = geometry.getJSONObject("location");
				double lat = location.getDouble("lat");
				double lng = location.getDouble("lng");
				return new LatLng(Double.toString(lat), Double.toString(lng));
			}
			LOG.info("Address \""+fullAddress+"\" location not found by Google Geocoder.");
			return null;
		}catch(JSONException e){
			LOG.info("Address \""+fullAddress+"\" location not found by Google Geocoder.");
			return null;
		}catch (IOException | URISyntaxException  e) {
			e.printStackTrace();
			return null;
		}
	}

	private boolean isLatLngInsideLimits(LatLng latLng, String latMax, String latMin, String lngMax, String lngMin){
		if(latLng!=null){
			double lat;
			double lng;
			double latMaxD;
			double latMinD;
			double lngMaxD;
			double lngMinD;

			try{
				 lat=Double.parseDouble(latLng.getLat());
				 lng=Double.parseDouble(latLng.getLng());
			}
			catch(Exception e){
				return false;
			}
			try{
				latMaxD=Double.parseDouble(latMax);
			}
			catch(Exception e){
				latMaxD=0;
			}
			try{
				latMinD=Double.parseDouble(latMin);

			}
			catch(Exception e){
				latMinD=0;
			}
			try{
				lngMaxD=Double.parseDouble(lngMax);
			}
			catch(Exception e){
				lngMaxD=0;
			}
			try{
				lngMinD=Double.parseDouble(lngMin);
			}
			catch(Exception e){
				lngMinD=0;
			}
			if(lat>latMinD && lat < latMaxD && lng > lngMinD && lng < lngMaxD ){
				return true;
			}
		}
		return false;
	}

	private  LatLng geocodingAddressOpenCage(String fullAddress, String APIKey) {
		try {
			//fullAddress=URLEncoder.encode(fullAddress,"UTF-8");
			String query = "q=" + fullAddress + "&key="+APIKey ;
			URI uriGoogleAPI = new URI("http", "api.opencagedata.com", "/geocode/v1/json", query, null);

			URL urlGoogleAPI = uriGoogleAPI.toURL();
//			try {
//				Thread.sleep(500);
//			} catch (InterruptedException e) {
//			}
			HttpURLConnection conn = (HttpURLConnection) urlGoogleAPI.openConnection();
			conn.setRequestMethod("GET");
			InputStream responseIS;
			try{
				cntRequest++;
				responseIS = conn.getInputStream();
			}
			catch (IOException e) {
				cntRequestKO++;
				return null;
			}

			StringBuilder result = new StringBuilder();
			BufferedReader rd = new BufferedReader(new InputStreamReader(responseIS));
			String line;
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			rd.close();

			JSONObject jsonResult = new JSONObject(result.toString());
			JSONArray results = jsonResult.getJSONArray("results");
			//looking for error
            /*if(jsonResult.getString("status").equals("OVER_QUERY_LIMIT")){
                throw new GoogleMapOverQueryLimitException();
            }
*/
			JSONObject result0 = results.getJSONObject(0);
			if (result0 != null) {
				JSONObject geometry = result0.getJSONObject("geometry");
				double lat = geometry.getDouble("lat");
				double lng = geometry.getDouble("lng");
				LatLng latLngObj = new LatLng(Double.toString(lat), Double.toString(lng));
				cntRequestOK++;
				return latLngObj;
			}
			LOG.info("Address \""+fullAddress+"\" location not found by OpenCage.");
			cntRequestKO++;
			return null;
		}catch(JSONException e){
			LOG.info("Address \""+fullAddress+"\" location not found by OpenCage.");
			return null;
		}catch (IOException | URISyntaxException  e) {
			e.printStackTrace();
			return null;
		}
	}


	private LatLng geocodingAddressGooglePlaces(String placeName, String placeCity, String APIKey) throws GoogleMapOverQueryLimitException {
		try {
			//placeCity="";
			String locationParam="&location=43.08 , 12.43 &radius=50000";
			String query = "query=" + URLEncoder.encode(placeName,"UTF-8") +", "+placeCity+ "&key="+APIKey ;
			URI uriGoogleAPI = new URI("https", "maps.googleapis.com", "/maps/api/place/textsearch/json", query, null);

			URL urlGoogleAPI = uriGoogleAPI.toURL();
//			try {
//				Thread.sleep(500);
//			} catch (InterruptedException e) {
//			}
			HttpURLConnection conn = (HttpURLConnection) urlGoogleAPI.openConnection();
			conn.setRequestMethod("GET");
			InputStream responseIS;
			try{
				cntRequest++;
				responseIS = conn.getInputStream();
			}
			catch (IOException e) {
				return null;
			}

			StringBuilder result = new StringBuilder();
			BufferedReader rd = new BufferedReader(new InputStreamReader(responseIS));
			String line;
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			rd.close();

			JSONObject jsonResult = new JSONObject(result.toString());
			JSONArray results = jsonResult.getJSONArray("results");
			//looking for error
			if(jsonResult.getString("status").equals("OVER_QUERY_LIMIT")){
				cntRequestKO++;
				throw new GoogleMapOverQueryLimitException();
			}

			JSONObject result0 = results.getJSONObject(0);
			if (result0 != null) {
				JSONObject geometry = result0.getJSONObject("geometry");
				JSONObject location = geometry.getJSONObject("location");
				double lat = location.getDouble("lat");
				double lng = location.getDouble("lng");
				LatLng latLngObj = new LatLng(Double.toString(lat), Double.toString(lng));
				cntRequestOK++;
				return latLngObj;
			}
			return null;
		} catch (IOException | URISyntaxException | JSONException e) {
			cntRequestKO++;
			e.printStackTrace();
			return null;
		}
	}
}
