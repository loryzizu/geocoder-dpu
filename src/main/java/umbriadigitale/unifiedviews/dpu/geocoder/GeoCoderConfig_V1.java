package umbriadigitale.unifiedviews.dpu.geocoder;


/**
 * Configuration class for GeoCoder.
 *
 * @author Lorenzo Franco Ranucci
 */
public class GeoCoderConfig_V1 {
	private boolean replace;
	private String latMax;
	private String latMin;
	private String lngMax;
	private String lngMin;
	/*Google*/
    private String APIKey;
	
	/*CSV*/
	private String addressColumnsIndexes;
    private String latColumnIndex;
    private String lngColumnIndex;
    private String separator;
    private boolean hasHeader;
    
    /*RDF*/
    private String addressPropertiesURIs;
    private String latPropertyURI;
    private String lngPropertyURI;
    
    

    public GeoCoderConfig_V1() {
    }


    public String getSeparator() {
        return separator;
    }

    public void setSeparator(String separator) {
        this.separator = separator;
    }

    public boolean hasHeader() {
        return hasHeader;
    }

    public void setHasHeader(boolean hasHeader) {
        this.hasHeader = hasHeader;
    }

	public String getAPIKey() {
		return APIKey;
	}

	public void setAPIKey(String aPIKey) {
		APIKey = aPIKey;
	}

	public boolean isReplace() {
		return replace;
	}

	public void setReplace(boolean replace) {
		this.replace = replace;
	}

	public String getAddressPropertiesURIs() {
		return addressPropertiesURIs;
	}

	public void setAddressPropertiesURIs(String addressPropertiesURIs) {
		this.addressPropertiesURIs = addressPropertiesURIs;
	}

	public String getLatPropertyURI() {
		return latPropertyURI;
	}

	public void setLatPropertyURI(String latPropertyURI) {
		this.latPropertyURI = latPropertyURI;
	}

	public String getLngPropertyURI() {
		return lngPropertyURI;
	}

	public void setLngPropertyURI(String lngPropertyURI) {
		this.lngPropertyURI = lngPropertyURI;
	}

	public boolean isHasHeader() {
		return hasHeader;
	}


	public String getAddressColumnsIndexes() {
		return addressColumnsIndexes;
	}


	public void setAddressColumnsIndexes(String addressColumnsIndexes) {
		this.addressColumnsIndexes = addressColumnsIndexes;
	}


	public String getLatColumnIndex() {
		return latColumnIndex;
	}


	public void setLatColumnIndex(String latColumnIndex) {
		this.latColumnIndex = latColumnIndex;
	}


	public String getLngColumnIndex() {
		return lngColumnIndex;
	}


	public void setLngColumnIndex(String lngColumnIndex) {
		this.lngColumnIndex = lngColumnIndex;
	}

	public String getLatMax() {
		return latMax;
	}

	public void setLatMax(String latMax) {
		this.latMax = latMax;
	}

	public String getLatMin() {
		return latMin;
	}

	public void setLatMin(String latMin) {
		this.latMin = latMin;
	}

	public String getLngMax() {
		return lngMax;
	}

	public void setLngMax(String lngMax) {
		this.lngMax = lngMax;
	}

	public String getLngMin() {
		return lngMin;
	}

	public void setLngMin(String lngMin) {
		this.lngMin = lngMin;
	}
}
