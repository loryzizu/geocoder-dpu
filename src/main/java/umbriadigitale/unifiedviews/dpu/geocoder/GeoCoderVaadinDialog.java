package umbriadigitale.unifiedviews.dpu.geocoder;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;

import eu.unifiedviews.dpu.config.DPUConfigException;
import eu.unifiedviews.helpers.dpu.vaadin.dialog.AbstractDialog;

/**
 * Vaadin configuration dialog for GeoCoder.
 *
 * @author Lorenzo Franco Ranucci
 */
@SuppressWarnings("serial")
public class GeoCoderVaadinDialog extends AbstractDialog<GeoCoderConfig_V1> {

    private VerticalLayout mainLayout;
    
    private TabSheet tabSheet;
    
    private CheckBox replace;
    private TextField APIKey;
    private TextField latMax;
    private TextField latMin;
    private TextField lngMax;
    private TextField lngMin;

    private TextField addressColumnsIndexes;
    private TextField latColumnIndex;
    private TextField lngColumnIndex;
    private TextField separator;
    private CheckBox hasHeader;
    
    private TextField addressPropertiesURIs;
    private TextField latPropertyURI;
    private TextField lngPropertyURI;
    
   
    

    
    

    public GeoCoderVaadinDialog() {
        super(GeoCoder.class);
    }

    @Override
    public void setConfiguration(GeoCoderConfig_V1 c) throws DPUConfigException {
    	this.replace.setValue(c.isReplace());
    	this.APIKey.setValue(c.getAPIKey());
        this.latMax.setValue(c.getLatMax());
        this.latMin.setValue(c.getLatMin());
        this.lngMax.setValue(c.getLngMax());
        this.lngMin.setValue(c.getLngMin());
    	
        this.addressColumnsIndexes.setValue(c.getAddressColumnsIndexes());
        this.latColumnIndex.setValue(c.getLatColumnIndex());
        this.lngColumnIndex.setValue(c.getLngColumnIndex());
        this.separator.setValue(c.getSeparator());
        this.hasHeader.setValue(c.hasHeader());
        
        
        this.addressPropertiesURIs.setValue(c.getAddressPropertiesURIs());
        this.latPropertyURI.setValue(c.getLatPropertyURI());
        this.lngPropertyURI.setValue(c.getLngPropertyURI());
        
    }

    @Override
    public GeoCoderConfig_V1 getConfiguration() throws DPUConfigException {
        final GeoCoderConfig_V1 c = new GeoCoderConfig_V1();
        
        c.setReplace(getReplace().getValue());
        c.setAPIKey(getAPIKey().getValue());
        c.setLatMax(getLatMax().getValue());
        c.setLatMin(getLatMin().getValue());
        c.setLngMax(getLngMax().getValue());
        c.setLngMin(getLngMin().getValue());
        
        c.setAddressColumnsIndexes(getAddressColumnsIndexes().getValue());
        c.setLatColumnIndex(getLatColumnIndex().getValue());
        c.setLngColumnIndex(getLngColumnIndex().getValue());
        c.setSeparator(getSeparator().getValue());
        c.setHasHeader(getHasHeader().getValue());
        
        
        c.setAddressPropertiesURIs(getAddressPropertiesURIs().getValue());
        c.setLatPropertyURI(getLatPropertyURI().getValue());
        c.setLngPropertyURI(getLngPropertyURI().getValue());
        
        return c;
    }

    @Override
    public void buildDialogLayout() {
        setWidth("100%");
        setHeight("100%");

        this.mainLayout = new VerticalLayout();
        this.mainLayout.setImmediate(false);
        this.mainLayout.setWidth("100%");
        this.mainLayout.setHeight("-1px");
        this.mainLayout.setSpacing(true);
        this.mainLayout.setMargin(true);
        
        tabSheet = new TabSheet();
        tabSheet.setSizeFull();
        tabSheet.setWidth("100%");
        tabSheet.setHeight("100%");
        
        
        /*
         * TAB General configs
         * */
        VerticalLayout generalLayout = new VerticalLayout();
        generalLayout.setWidth("100%");
        generalLayout.addComponent(new Label("&nbsp;", ContentMode.HTML));
        
        this.APIKey= new TextField();
        this.APIKey.setCaption("Google Maps Geocoding API Key");
        this.APIKey.setNullRepresentation("");
        this.APIKey.setWidth("100%");
        generalLayout.addComponent(this.APIKey);
        generalLayout.addComponent(new Label("&nbsp;", ContentMode.HTML));
        
        this.replace= new CheckBox();
        this.replace.setCaption("Sostituisci");
        this.replace.setDescription("Sostituisci i valori di latitudine e longitudine già esistenti");
        generalLayout.addComponent(this.replace);
        generalLayout.addComponent(new Label("&nbsp;", ContentMode.HTML));

        this.latMax= new TextField();
        this.latMax.setCaption("Lat max");
        this.latMax.setNullRepresentation("");
        this.latMax.setWidth("100%");
        generalLayout.addComponent(this.latMax);
        generalLayout.addComponent(new Label("&nbsp;", ContentMode.HTML));

        this.latMin= new TextField();
        this.latMin.setCaption("Lat min");
        this.latMin.setNullRepresentation("");
        this.latMin.setWidth("100%");
        generalLayout.addComponent(this.latMin);
        generalLayout.addComponent(new Label("&nbsp;", ContentMode.HTML));

        this.lngMax= new TextField();
        this.lngMax.setCaption("Lng max");
        this.lngMax.setNullRepresentation("");
        this.lngMax.setWidth("100%");
        generalLayout.addComponent(this.lngMax);
        generalLayout.addComponent(new Label("&nbsp;", ContentMode.HTML));

        this.lngMin= new TextField();
        this.lngMin.setCaption("Lng min");
        this.lngMin.setNullRepresentation("");
        this.lngMin.setWidth("100%");
        generalLayout.addComponent(this.lngMin);
        generalLayout.addComponent(new Label("&nbsp;", ContentMode.HTML));
        
        tabSheet.addTab(generalLayout, ctx.tr("GeoCoder.dialog.labelGeneral"));
        
        /*
         * TAB CSV configs
         * */
        VerticalLayout csvLayout = new VerticalLayout();
        csvLayout.setWidth("100%");
        csvLayout.addComponent(new Label("&nbsp;", ContentMode.HTML));

        this.addressColumnsIndexes = new TextField();
        this.addressColumnsIndexes.setCaption("Indici colonne indirizzo");
        this.addressColumnsIndexes.setDescription("Indici delle colonne che compongono l'indirizzo, separati da virgola");
        this.addressColumnsIndexes.setWidth("100%");
        csvLayout.addComponent(this.addressColumnsIndexes);
        csvLayout.addComponent(new Label("&nbsp;", ContentMode.HTML));
        
        this.latColumnIndex = new TextField();
        this.latColumnIndex.setCaption("Indice colonna latitudine");
        this.latColumnIndex.setDescription("Indice della colonna che contiene la latitudine nel file di input");
        this.latColumnIndex.setWidth("100%");
        csvLayout.addComponent(this.latColumnIndex);
        csvLayout.addComponent(new Label("&nbsp;", ContentMode.HTML));
        
        this.lngColumnIndex = new TextField();
        this.lngColumnIndex.setCaption("Indice colonna longitudine");
        this.lngColumnIndex.setDescription("Indice della colonna che contiene la longitudine nel file di input");
        this.lngColumnIndex.setWidth("100%");
        csvLayout.addComponent(this.lngColumnIndex);
        csvLayout.addComponent(new Label("&nbsp;", ContentMode.HTML));

        this.separator= new TextField();
        this.separator.setCaption("Separatore");
        this.separator.setWidth("100%");
        csvLayout.addComponent(this.separator);
        csvLayout.addComponent(new Label("&nbsp;", ContentMode.HTML));

        this.hasHeader= new CheckBox();
        this.hasHeader.setCaption("Il CSV ha uno header");
        csvLayout.addComponent(this.hasHeader);
        
        tabSheet.addTab(csvLayout, ctx.tr("GeoCoder.dialog.labelCSV"));
        
        
        /*
         * TAB RDF configs
         * */
        VerticalLayout rdfLayout = new VerticalLayout();
        rdfLayout.setWidth("100%");
        rdfLayout.addComponent(new Label("&nbsp;", ContentMode.HTML));
        
        this.addressPropertiesURIs= new TextField();
        this.addressPropertiesURIs.setCaption("URIs predicati indirizzo");
        this.addressPropertiesURIs.setDescription("URI dei predicati i cui oggetti compongono l'indirizzo. Separati da virgola.");
        this.addressPropertiesURIs.setWidth("100%");
        rdfLayout.addComponent(this.addressPropertiesURIs);
        rdfLayout.addComponent(new Label("&nbsp;", ContentMode.HTML));
        
        this.latPropertyURI= new TextField();
        this.latPropertyURI.setCaption("URI predicato latitudine");
        this.latPropertyURI.setDescription("URI del predicato il cui oggetto contiene la latitudine (sia in input che in output)");
        this.latPropertyURI.setWidth("100%");
        rdfLayout.addComponent(this.latPropertyURI);
        rdfLayout.addComponent(new Label("&nbsp;", ContentMode.HTML));
        
        this.lngPropertyURI= new TextField();
        this.lngPropertyURI.setCaption("URI predicato longitudine");
        this.lngPropertyURI.setDescription("URI del predicato il cui oggetto contiene la longitudine (sia in input che in output)");
        this.lngPropertyURI.setWidth("100%");
        rdfLayout.addComponent(this.lngPropertyURI);
        rdfLayout.addComponent(new Label("&nbsp;", ContentMode.HTML));
        
        tabSheet.addTab(rdfLayout, ctx.tr("GeoCoder.dialog.labelRDF"));
        

        mainLayout.addComponent(tabSheet);
        Panel panel = new Panel();
        panel.setSizeFull();
        panel.setContent(this.mainLayout);
        setCompositionRoot(panel);
    }


    public TextField getSeparator() {
        return separator;
    }

    public void setSeparator(TextField separator) {
        this.separator = separator;
    }

    public CheckBox getHasHeader() {
        return hasHeader;
    }

    public void setHasHeader(CheckBox hasHeader) {
        this.hasHeader = hasHeader;
    }

	public TextField getAPIKey() {
		return APIKey;
	}

	public void setAPIKey(TextField aPIKey) {
		APIKey = aPIKey;
	}

	public TextField getAddressPropertiesURIs() {
		return addressPropertiesURIs;
	}

	public void setAddressPropertiesURIs(TextField addressPropertiesURIs) {
		this.addressPropertiesURIs = addressPropertiesURIs;
	}

	public TextField getLatPropertyURI() {
		return latPropertyURI;
	}

	public void setLatPropertyURI(TextField latPropertyURI) {
		this.latPropertyURI = latPropertyURI;
	}

	public TextField getLngPropertyURI() {
		return lngPropertyURI;
	}

	public void setLngPropertyURI(TextField lngPropertyURI) {
		this.lngPropertyURI = lngPropertyURI;
	}

	public CheckBox getReplace() {
		return replace;
	}

	public void setReplace(CheckBox replace) {
		this.replace = replace;
	}

	public TextField getAddressColumnsIndexes() {
		return addressColumnsIndexes;
	}

	public void setAddressColumnsIndexes(TextField addressColumnsIndexes) {
		this.addressColumnsIndexes = addressColumnsIndexes;
	}

	public TextField getLatColumnIndex() {
		return latColumnIndex;
	}

	public void setLatColumnIndex(TextField latColumnIndex) {
		this.latColumnIndex = latColumnIndex;
	}

	public TextField getLngColumnIndex() {
		return lngColumnIndex;
	}

	public void setLngColumnIndex(TextField lngColumnIndex) {
		this.lngColumnIndex = lngColumnIndex;
	}

    public TextField getLatMax() {
        return latMax;
    }

    public void setLatMax(TextField latMax) {
        this.latMax = latMax;
    }

    public TextField getLatMin() {
        return latMin;
    }

    public void setLatMin(TextField latMin) {
        this.latMin = latMin;
    }

    public TextField getLngMax() {
        return lngMax;
    }

    public void setLngMax(TextField lngMax) {
        this.lngMax = lngMax;
    }

    public TextField getLngMin() {
        return lngMin;
    }

    public void setLngMin(TextField lngMin) {
        this.lngMin = lngMin;
    }
}
