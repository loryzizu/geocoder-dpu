package umbriadigitale.unifiedviews.dpu.geocoder.exception;

@SuppressWarnings("serial")
public class GoogleMapOverQueryLimitException extends Exception {
	private static final String MESSAGE="";
	public GoogleMapOverQueryLimitException(){
		super(GoogleMapOverQueryLimitException.MESSAGE);
	}
}
