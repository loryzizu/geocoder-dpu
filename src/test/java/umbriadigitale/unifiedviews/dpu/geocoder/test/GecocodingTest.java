package umbriadigitale.unifiedviews.dpu.geocoder.test;

import cz.cuni.mff.xrg.odcs.dpu.test.TestEnvironment;
import eu.unifiedviews.dataunit.files.FilesDataUnit;
import eu.unifiedviews.dataunit.files.WritableFilesDataUnit;
import eu.unifiedviews.helpers.dpu.test.config.ConfigurationBuilder;
import org.junit.BeforeClass;
import org.junit.Test;
import umbriadigitale.unifiedviews.dpu.geocoder.GeoCoder;
import umbriadigitale.unifiedviews.dpu.geocoder.GeoCoderConfig_V1;

/**
 * Created by Lorenzo Ranucci on 12/01/2017.
 */
public class GecocodingTest {

    private static TestEnvironment testEnv;
    private static GeoCoder dpu;

    private static FilesDataUnit filesDataUnitInput;
    public static WritableFilesDataUnit filesOutput;
    public static WritableFilesDataUnit filesOutput2;


    @BeforeClass
    public static  void before() throws Exception {
        dpu = new GeoCoder();
        testEnv = new TestEnvironment();
        testEnv.createFilesInput("Input");
        filesDataUnitInput=testEnv.createFilesInput("Input");
        filesOutput = testEnv.createFilesOutput("gecocodedResourcesFile");
        filesOutput2 = testEnv.createFilesOutput("discardedResourcesFile");
        ((WritableFilesDataUnit)filesDataUnitInput).addExistingFile("elenco_strutture_ricettive_geocoded_small.rdf", GecocodingTest.class.getClassLoader().getResource("elenco_strutture_ricettive_geocoded.rdf").toURI().toString());
    }

    @Test
    public void geocodeRdf() throws Exception {
        GeoCoderConfig_V1 config = new GeoCoderConfig_V1();

        /*generic configs*/
        config.setAPIKey("AIzaSyBq198PF3NeebU4rMxraHGP0Sik5EWGR_o");
        config.setReplace(false);
        config.setLatMax("43.618180");
        config.setLatMin("42.364552");
        config.setLngMax("13.275647");
        config.setLngMin("11.848184");

        /*rdf specific configs*/
        config.setAddressPropertiesURIs("http://schema.org/streetAddress , http://schema.org/addressLocality , http://schema.org/addressRegion");
        config.setLatPropertyURI("http://www.w3.org/2003/01/geo/wgs84_pos#lat");
        config.setLngPropertyURI("http://www.w3.org/2003/01/geo/wgs84_pos#long");


        this.dpu.configure((new ConfigurationBuilder()).setDpuConfiguration(config).toString());
        this.testEnv.run(this.dpu);
        this.testEnv.getContext().getResultDir();
    }
}
